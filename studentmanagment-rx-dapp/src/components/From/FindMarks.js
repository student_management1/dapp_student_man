import React, { Component } from "react";
import './Form.css'
class MarksList extends Component {
  //Initialise the variables stored in the state
  constructor(props) {
    super(props);
    this.state = {
      marks: "",
    };
  }
  render() {
    return (
      <form
        className="p-5 border"
        onSubmit={(event) => {
          event.preventDefault();
          this.props
            .findMarks(this.studentid.value, this.subjectid.value)
            .then((result) => {
              this.setState({ marks: result.grade });
            });
        }}
      >
        <h2>Find Marks</h2>
        <select
          id="studentid"
          className="form-select mb-2"
          ref={(input) => {
            this.studentid = input;
          }}
        >
          <option defaultValue={true}>Open this select menu</option>
          {this.props.students.map((student, key) => {
            return (
              <option key={key} value={student.cid}>
                {student.cid}
                {student.name}
              </option>
            );
          })}
        </select>
        <select
          id="subjectid"
          className="form-select mb-2"
          ref={(input) => {
            this.subjectid = input;
          }}
        >
          <option defaultValue={true}>Open this select menu</option>
          {this.props.subjects.map((subject, key) => {
            return (
              <option key={key} value={subject.code}>
                {subject.code}
                {subject.name}
              </option>
            );
          })}
        </select>
        {/* <input
          className="form-control btn btn-primary"
          type="submit"
          hidden=""
        /> */}
          <button className='btn btn-info' type="submit" hidden="" style={{ width: "7cm" }}>Submit</button>

        <p>Grade : {this.state.marks}</p>
      </form>
    );
  }
}
export default MarksList;
