
import StudentList from "./components/From/StudentList";
import { STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS } from "./abi/config_studentlist";

import { MARK_LIST_ABI, MARK_LIST_ADDRESS} from "./abi/config_marklist";

// import { SUBJECT_LIST_ABI, SUBJECT_LIST_ADDRESS } from "./abi/config_subjectlist";

import { SUBJECT_LIST_ABI, SUBJECT_LIST_ADDRESS } from "./abi/config_subjectlist";



import SubjectForm from "./components/From/SubjectForm";
import Marklist from "./components/From/MarkList";
import FindMarks from  './components/From/FindMarks'

import React, { Component} from "react";
import Web3 from 'web3';
import { Modal, Button } from "react-bootstrap";

class App extends Component {
  state = {
    isOpen: false
  };

  openModal = () => this.setState({ isOpen: true });
  closeModal = () => this.setState({ isOpen: false });


  componentDidMount() {
    if (!window.ethereum)
      throw new Error("No wallet found, Please install it");
    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData()
    this.loadSubjetData()
    this.loadMarkData()
  }

  async loadBlockchainData() {
    const web3 = new Web3(Web3.givenProvider || "https://sepolia.infura.io/v3/3b8a1d11bec9442abe0bb76a3f735ab8");
    const accounts = await web3.eth.getAccounts();
    this.setState({ account: accounts[0] });
  
    // Load all the lists from the blockchain
    const StudentList = new web3.eth.Contract(STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS);
    this.setState({ StudentList });
  
    // Get the number of records for all lists in the blockchain
    const studentCount = await StudentList.methods.studentsCount().call();
    this.setState({ studentCount });
  
    const students = [];
    for (let i = 1; i <= studentCount; i++) {
      const student = await StudentList.methods.students(i).call();
      students.push(student);
    }
    this.setState({ students });
  }
  

  // For SubjectData
  async loadSubjetData() {
    const web3 = new Web3(Web3.givenProvider || "https://sepolia.infura.io/v3/3b8a1d11bec9442abe0bb76a3f735ab8");
    const subjectList = new web3.eth.Contract(SUBJECT_LIST_ABI, SUBJECT_LIST_ADDRESS);
    this.setState({ subjectList });
  
    const subjectCount = await subjectList.methods.subjectsCount().call();
    this.setState({ subjectCount });
  
    const subjects = [];
    for (let i = 1; i <= subjectCount; i++) {
      const subject = await subjectList.methods.subjects(i).call();
      subjects.push(subject);
    }
    this.setState({ subjects });
  }
  

  async loadMarkData(){
    const web3 = new Web3(Web3.givenProvider ||  "https://sepolia.infura.io/v3/3b8a1d11bec9442abe0bb76a3f735ab8")
    const marklist = new web3.eth.Contract(
      MARK_LIST_ABI, MARK_LIST_ADDRESS)
      this.setState({marklist})
      const markCount = await marklist.methods.marksCount().call()
      this.setState({markCount})
      // this.state.marks = []
      // for (var i = 1; i<=markCount; i++){
      //   const mark = await marklist.methods.marks(i).call();
      //   this.setState({
      //     marks: [...this.state.marks, mark]
      //   })
      // }
    
  }

  constructor(props) {
    super(props)
    this.state = {
      account: "",
      studentCount : 0,
      subjectCount: 0,
      markCount:0,
      subjects: [],
      students: [],
      marks:[],
      loading: true,
      id: 0,
      name: "",
      cid: ""
    }
    // for fetching form imput for all student starts
    this.createStudent = this.createStudent.bind(this)
    this.updateStudent = this.updateStudent.bind(this)
    this.markGraduated = this.markGraduated.bind(this)
    this.createSubject= this.createSubject.bind(this)
    this.createMark = this.createMark.bind(this)
    this.findMarks = this.findMarks.bind(this)

  }
  createStudent(cid, name) {
    // alert("amsdnhkjasnd,mn")
    this.setState({ loading: true })
    this.state.StudentList.methods.createStudent(cid, name)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }
  updateStudent(id, name, cid) {
    this.setState({ loading: true })
    this.state.StudentList.methods.updateStudent(id, name, cid)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })

  }

   // Updating Student Marks
  updateMarks(cid,code,newgrade){
    this.setState({loading: true})
    this.state.MarkList.methods.updateMarks(cid,code,newgrade)
    .send({from:this.state.account})
    .once('receipt', (receipt)=>{
      this.setState({loading:false})
      this.loadMarkData()
    })
  }
  
  markGraduated(cid){
    this.setState({loading: true})
    this.state.studentList.methods.markGraduated(cid)
      .send({from: this.state.account})
      .once('receipt', (receipt) => {
        this.setState({loading: false})
        this.loadBlockchainData()
      })
  }
  updateState(newState){
    this.setState(newState);

  }

  createSubject(code, subject) {
    // alert("amsdnhkjasnd,mn")
    this.setState({ loading: true })
    this.state.subjectList.methods.createSubject(code,subject)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadSubjetData()
      })
  }


  createMark(studentid, subjectid, grades) {
    this.setState({ loading: true })
    this.state.marklist.methods.addMarks(studentid,subjectid, grades)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadMarkData()
      })
  }

  findMarks(studentid, subjectid) {
    this.setState({ loading: true })
    return this.state.marklist.methods.findMarks(studentid, subjectid)
    .call({ from: this.state.account })
    }
    

  
  // ends



  render() {
    const handlePopFunction = (e) => {
      e.preventDefault()
      this.updateStudent(this.state.id, this.state.name, this.state.cid)
    }

    return (
      <>
        <div className="container">
          {/* <h1>Hello world</h1> */}


          <p>your account:{this.state.account}</p>
          <StudentList createStudent={this.createStudent} />
          <ul id='studentlist' className="list-unstyle">{this.state.students.map((student, key) => {
            return (
              <li className=" list-group-item checkbox" key={key}>
                <span className="'name alert">{student._id}. {student.cid} {student.name}</span>
                <input className="'form-check-input" type="checkbox" name={student._id} defaultChecked={student.graduated} disabled={student.graduated} ref={(input) => {
                  this.checkbox = input;
                }} />
                <label className="'form-check-label">
                  Graduated
                </label>

                {/* <Button size="sm" style={{ border: "none", paddingRight: 2 }} onClick={(e) => (
                  handleupdatePopUp(e, student))}>
                  Update
                </Button> */}
                <Button size="sm" style={{ border: "none", paddingRight: 2 }} onClick={(e) => {
                  this.openModal();
                  this.setState({ id: student._id, name: student.name, cid: student.cid })
                }}>
                  Update
                </Button>

              </li>

            );
          })}
          </ul>

          <p>TotalStudent:{this.state.studentCount}</p>

            {/* for subject list */}
        <SubjectForm createSubject={this.createSubject}/>


<ul id='subjectlist' className='list-unstyled'>
{
  this.state.subjects.map((subject, key) => {
    return(
      <li className='list-group-item checkbox' key={key}>
        <span className='name alert'> {subject._id} {subject.code} {subject.subject} </span>
        <input className='form-check-input' type='checkbox' name={subject._id} defaultChecked ={subject.retired} 
        disabled={subject.retired} ref={(input) => {
          this.checkbox = input
        }} 
        />
        <label className='form-check-label'>retired</label>

      </li>

    )
  })
}  
</ul>  
<p>TotalSubject:{this.state.subjectCount}</p>


 {/* Marklist */}


  {/* <ul id='subjectlist' className='list-unstyled'>
{
  this.state.marks.map((mark, key) => {
    return(
      <li className='list-group-item checkbox' key={key}>
        <span className='name alert'> {mark._id} {mark.code} {mark.mark} </span> 


     
        <button type="submit" className="btn btn-success" style={{ marginTop: '10px' }} onClick={()=>{this.setState({update:true})
        this.studentObj = student
        }} >Alter</button>
        
      </li>

    )
  })
}  
</ul>   */}

<Marklist 
subjects={this.state.subjects}
students ={this.state.students} 
createMark ={this.createMark}/>
<p>MarksCount:{this.state.markCount}</p> 


        </div >

         <FindMarks
subjects = {this.state.subjects}
students = {this.state.students}
findMarks={this.findMarks}
/> 
        
        <Modal show={this.state.isOpen} onHide={this.closeModal}>
          <Modal.Header closeButton>  <h2 style={{ textAlign: "center" }}>Update Student</h2>
          </Modal.Header>
          <form className="form p-5 border" onSubmit={(event) => {
            event.preventDefault()
            this.props.addStudent(this.sid.value, this.student.value)
          }}>
            <br></br>
            <br></br>
            <input id="" type="number"
              className="form-control m-1"
              placeholder="CID"
              required
              value={this.state.cid}
              onChange={(event) => this.setState({ cid: event.target.value })}

            />
            <br></br>
            <br></br>
            <input id="newStudent" type="text"
              className="form-control m-1"
              placeholder="Student Name"
              required
              value={this.state.name}
              onChange={(e) => this.setState({ name: e.target.value })}

            />
            <br></br>
            <br></br>
            <button onClick={(e) => handlePopFunction(e)} className='btn btn-info' type="submit" hidden="" style={{ width: "5.5cm" }}>Submit</button>

            {/* <input className="form-control btn-primary" type="submit" hidden="" style={{ width: "7cm" }} /> */}
          </form>
        </Modal>

           {/* modal for Updating Marks */}
       
           <Modal show={this.state.isOpen} onHide={this.closeModal}>
          <Modal.Header closeButton>  <h2 style={{ textAlign: "center" }}>Update Student</h2>
          </Modal.Header>
          <form className="form p-5 border" onSubmit={(event) => {
            event.preventDefault()
            this.props.addStudent(this.sid.value, this.student.value)
          }}>
            <br></br>
            <br></br>
            <input id="" type="number"
              className="form-control m-1"
              placeholder="CID"
              required
              value={this.state.cid}
              onChange={(event) => this.setState({ cid: event.target.value })}

            />
            <br></br>
            <br></br>
            <input id="newStudent" type="text"
              className="form-control m-1"
              placeholder="Student Name"
              required
              value={this.state.name}
              onChange={(e) => this.setState({ name: e.target.value })}

            />
            <br></br>
            <br></br>
            <button onClick={(e) => handlePopFunction(e)} className='btn btn-info' type="submit" hidden="" style={{ width: "5.5cm" }}>Submit</button>

            {/* <input className="form-control btn-primary" type="submit" hidden="" style={{ width: "7cm" }} /> */}
          </form>
        </Modal>
      
        </>


    )
  }
}
export default App;



